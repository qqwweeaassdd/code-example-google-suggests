'use strict';

const config = require('config');
const suggest = require('node-google-suggest-en');
const he = require('he');

const db = require('./models')(config.get('name'));

let charList = '0123456789abcdefghijklmnopqrstuvwxyz';


let stripTagsRegex = /\<\/?[^\>]*\>/g;
let stripTags = str => str.replace(stripTagsRegex, '');

/**
 * Process keyword group: base + charList[idx]
 * @param keyword string base keyword
 * @returns {Promise}
 */
function processKeywordGroup(keyword) {
    return new Promise(resolve => {

        let suggests = {};

        function processKeyword(delay) {
            if (testKeywords.length === 0) {
                let list = Object.keys(suggests)
                    .map(item => he.decode(stripTags(item)));

                return resolve({
                    suggests:       list,
                    testedKeywords: testedKeywords
                });
            }

            setTimeout(function () {
                suggest(testKeywords.shift(), (err, res) => {
                    if (err || !res) {
                        console.error(err || 'No err');
                        console.log('Set long delay 10m');
                        return processKeyword(10 * 60 * 1000);
                    }

                    res.forEach(suggest => {
                        suggests[suggest] = true;
                    });

                    return processKeyword(750 + Math.random() * 1000);
                });
            }, delay || 750);
        }

        // get not processed keywords
        let testKeywords = [keyword, `${keyword} `];
        let testedKeywords;

        let promises = [
            db.Keyword.checkDone(testKeywords), // check with readiness
            db.Used.checkDone(testKeywords) // check with used
        ];

        Promise.all(promises).then(datas => {

            // take datas[0] & datas[1] intersection
            let objs = {};
            datas[1].forEach(item => {
                objs[item] = true;
            });
            testKeywords = datas[0].filter(item => (objs[item] === true) );

            testedKeywords = [].concat(testKeywords);

            if (testKeywords.length === 0) {
                return resolve({
                    suggests:       [],
                    testedKeywords: []
                });
            }

            return processKeyword();
        }).catch(err => {
            throw err;
        });
    });
}

/**
 * Find suggests for keyword
 */
function runKeyword() {
    let thisKeyword;

    // get next keyword
    db.Keyword.getNext().then(keyword => {
        console.log('--> ' + keyword);

        // no keywords
        if (!keyword) {
            throw true;
        }

        // get suggests
        thisKeyword = keyword;
        return processKeywordGroup(keyword);
    }).then(data => {

        return Promise.all([
            db.Keyword.saveList(data.suggests),    // save keywords
            db.Used.saveList(data.testedKeywords), // save used variants
            db.Keyword.finishKeyword(thisKeyword)  // save checked status
        ]);
    }).then(() => {

        console.log(`+++ ${thisKeyword}`);
        process.nextTick(runKeyword);
    }).catch(err => {

        if (err === true) {
            console.log('Finished');
        } else {
            throw err;
        }
    });
}

// start
db.sequelize.sync()
    .then(runKeyword)
    .catch(err => {
        throw err;
    });
