'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

let basename = path.basename(module.filename);
let env = process.env.NODE_ENV || 'development';

let dbs = {};

/**
 * Returns DB connection
 * @param dbName string
 * @returns {Object}
 */
module.exports = (dbName) => {

    if (typeof dbs[dbName] !== 'undefined') {
        return dbs[dbName];
    }

    let logging = false;
    let sequelize;
    let db = {};
    let storage = `./storage/base-${dbName}.sqlite`;
    let config = {
        dialect: 'sqlite',
        storage: storage,
        define: {
            timestamps: false,
            freezeTableName: true
            }
        };

    try {
        logging = (require('config').get('db.logging') === true);
    } catch (err) { }

    if (env == 'production' || !logging) {
        config.logging = false;
    }
    sequelize = new Sequelize(config);

    fs.readdirSync(__dirname)
        .filter(file => {
            return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
        }).forEach(file => {
            let model = sequelize['import'](path.join(__dirname, file));
            db[model.name] = model;
        });

    Object.keys(db).forEach(modelName => {
        if (db[modelName].associate) {
            db[modelName].associate(db);
        }
    });

    db.sequelize = sequelize;
    db.Sequelize = Sequelize;

    dbs[dbName] = db;

    return db;
};
