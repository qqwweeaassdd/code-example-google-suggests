'use strict';

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Keyword', {
        id: {
            type:          DataTypes.INTEGER,
            allowNull:     false,
            primaryKey:    true,
            autoIncrement: true
        },
        keyword: {
            type:      DataTypes.STRING,
            unique:    true,
            allowNull: false
        },
        ready: {
            type:      DataTypes.INTEGER
        }
    }, {
        createdAt:       false,
        updatedAt:       false,
        freezeTableName: true,
        classMethods:    {

            /**
             * Returns not checked keyword of required type
             * @returns {Promise}
             */
            getNext: function () {
                let params = {
                    attributes: ['keyword'],
                    where: {
                        ready: 0
                    },
                    order: ['id DESC', 'id ASC'][Math.round(Math.random())] // random sort
                };

                return this.findOne(params)
                    .then(res => res.get().keyword);
            },

            /**
             * Check keyword
             * @param keyword string
             * @returns {Promise}
             */
            finishKeyword: function (keyword) {
                let params = {
                    where: {
                        keyword: keyword
                    }
                };

                return this.update({ ready: 1 }, params);
            },

            /**
             * Check if keywords are checked
             * @param testKeywords array
             * @returns {Promise}
             */
            checkDone: function (testKeywords) {
                let params = {
                    attributes: ['keyword'],
                    where: {
                        keyword: {
                            $in: testKeywords
                        },
                        ready: 0
                    }
                };

                return this.findAll(params).then(res => {

                    let readyKeywords = res.map(item => item.get().keyword);
                    testKeywords = testKeywords.filter(item => readyKeywords.indexOf(item) < 0);
                    return testKeywords;
                });
            },

            /**
             * Save new keywords to DB
             * @param keywords array
             * @returns {Promise}
             */
            saveList: function (keywords) {
                // Get unique
                let keywordsObj = {};
                keywords.map(item => item.trim())
                    .filter(item => item)
                    .forEach(item => {
                        keywordsObj[item] = true;
                    });
                keywords = Object.keys(keywordsObj);

                // Get existing keywords
                let params = {
                    attributes: ['keyword'],
                    where: {
                        keyword: {
                            $in: keywords
                        }
                    }
                };

                return this.findAll(params).then(res => {

                    // filter existing
                    let keywordsInBase = res.map(item => item.get().keyword);
                    keywords = keywords.filter(item => keywordsInBase.indexOf(item) < 0);

                    // bulk save
                    let items = keywords.map(item => ({
                        keyword: item,
                        ready:   0
                    }));

                    return this.bulkCreate(items);
                });
            }
        }
    });
};
